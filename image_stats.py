import os
from skimage.io import imread
import numpy as np
from skimage.io import imread
from skimage import filters
from skimage.color import rgb2gray
from skimage.feature import hog
import cv2, pickle, zipfile, io



def structure_data(unstructured_data_path, upload_dir, file_name):
    labels = [i for i in os.listdir(unstructured_data_path) if os.path.isdir(os.path.join(unstructured_data_path,i))]
    if len(labels) == 1 or ("__MACOSX" in labels and len(labels) == 2):
        if "__MACOSX" in labels:
            labels.remove("__MACOSX")
        search_directory = os.path.join(unstructured_data_path, labels[0])
        labels = [i for i in os.listdir(search_directory) if os.path.isdir(os.path.join(search_directory,i))]
        return get_image_stats(search_directory, labels, upload_dir, file_name)
    elif len(labels) < 1:
        raise Exception("Dataset only contains one folder")
    else:
        return get_image_stats(unstructured_data_path, labels, upload_dir, file_name)


def get_mean_std(data, column, index=None):
    selected_data = []
    
    for i in range(len(data)):
        if index is None:
            if isinstance(data[i][column], np.ndarray):
                for j in data[i][column].flatten():
                    selected_data.append(j)
            else:
                selected_data.append(i)
        else:
            selected_data.append(i)
    standard_deviation = np.std(selected_data)
    mean = np.mean(selected_data)
    return (standard_deviation, mean)

def get_image_stats(unstructured_data_path, labels, upload_dir, file_name):
    
    with open("training_data/network_arch.pkl", "rb") as mypicklefile:
        data = pickle.load(mypicklefile)
    total_images = 0
    edges_std_rec, edges_mean_rec = get_mean_std(data, "edges")
    dataset_size_std_rec, dataset_size_mean_rec = get_mean_std(data, "dataset_size")
    compressed_dataset_size_std_rec, compressed_dataset_size_mean_rec = get_mean_std(data, "compressed_dataset_size")
    labels_amount_std_rec, labels_amount_mean_rec = get_mean_std(data, "labels_amount")
    width_dimensions_std_rec, width_dimensions_mean_rec = get_mean_std(data, "image_dimensions", 0)
    height_dimensions_std_rec, height_dimensions_mean_rec = get_mean_std(data, "image_dimensions", 1)
    images_amount_std_rec, images_amount_mean_rec = get_mean_std(data, "images_amount")
    mean_red_std_rec, mean_red_mean_rec = get_mean_std(data, "mean_red")
    mean_green_std_rec, mean_green_mean_rec = get_mean_std(data, "mean_green")
    mean_blue_std_rec, mean_blue_mean_rec = get_mean_std(data, "mean_blue")

    red_compiled_signal = None
    green_compiled_signal = None
    blue_compiled_signal = None
    edges_compiled_signal = None
    img_shape = None

    edge_data = []
    color_data = []
    dataset_size = 0
    compressed_dataset_size = 0

    memory_file = io.BytesIO()
    
    with zipfile.ZipFile(memory_file, "w", zipfile.ZIP_DEFLATED) as zf:
        for label in labels:
            directory = os.path.join(unstructured_data_path, label)
            for file in os.listdir(directory):
                if not file.startswith("."):
                    zf.write(os.path.join(directory, file), file)
                    im = cv2.imread(os.path.join(directory, file), 1)
                    total_images+=1
                    if img_shape is None:
                        img_shape = im.shape

                    color_data.append(im)
                    edge_data.append(filters.sobel(rgb2gray(np.array(im, copy=True))))
    for info in zf.infolist():
        dataset_size += info.file_size
        compressed_dataset_size += info.compress_size

    

    edge_mean = np.mean(edge_data, axis=0)
    arr = np.mean(color_data, axis=0)
    record = list(
        np.concatenate(
            [
                ((arr[:,:,0].flatten() - mean_red_mean_rec) / mean_red_std_rec),
                ((arr[:,:,1].flatten() - mean_green_mean_rec) / mean_green_std_rec),
                ((arr[:,:,2].flatten() - mean_blue_mean_rec) / mean_blue_std_rec),
                ((np.array(edge_data).flatten() - edges_mean_rec) / edges_std_rec)
            ]
        )
    )

    size = 500
    pooling_window_size = len(record) / size
    pooling_window_remainder = len(record) % size
    pooled_record = []
    for j in range(0, size):
        current_window = record[j*int(pooling_window_size): (j * int(pooling_window_size)) + int(pooling_window_size)]
        pooled_record.append(max(current_window))
    
    current_window = record[(j * int(pooling_window_size)) + int(pooling_window_size): ]
    pooled_record.append(max(current_window))

    width = img_shape[0]
    height = img_shape[1]

    pooled_record.append((dataset_size - dataset_size_mean_rec) / (dataset_size_std_rec))
    pooled_record.append((compressed_dataset_size - compressed_dataset_size_mean_rec) / compressed_dataset_size_std_rec)
    pooled_record.append((len(labels) - labels_amount_mean_rec) / labels_amount_std_rec)
    pooled_record.append((total_images - images_amount_mean_rec) / images_amount_std_rec)
    pooled_record.append((width - width_dimensions_mean_rec) / width_dimensions_std_rec)
    pooled_record.append((height - height_dimensions_mean_rec) / height_dimensions_std_rec)

    
    
    return np.array(pooled_record)